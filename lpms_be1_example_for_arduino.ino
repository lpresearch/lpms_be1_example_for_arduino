#include "Arduino.h"
#include "lpms_be1.h"


LPMS_BE1 lpms_be1;
sensor_data_t sensor_data;


void be1_config_test(void);
void be1_read_data(void);
void be1_print_data(void);


void setup() {
    Serial.begin(115200);
    lpms_be1.setup();
}

void loop() {
    be1_read_data();
    be1_print_data();
    delay(1000);
}



void be1_config_test(void)
{
    uint8_t buf[9] = { 0 };
    lpms_be1.get_reg_data(0x00, buf, 9);
    for (int i = 0; i < 9; i++) {
        if (buf[i] < 0x10) {
            Serial.print("0");
            Serial.print(buf[i], HEX);
        } else {
            Serial.print(buf[i], HEX);
        }
        Serial.print(" ");
        
    }
    Serial.println();

    lpms_be1.setup();

    lpms_be1.get_reg_data(0x00, buf, 9);
    for (int i = 0; i < 9; i++) {
        if (buf[i] < 0x10) {
            Serial.print("0");
            Serial.print(buf[i], HEX);
        } else {
            Serial.print(buf[i], HEX);
        }
        Serial.print(" ");
    }
    Serial.println();
    Serial.println();
}

void be1_read_data(void)
{
    lpms_be1.get_timestamp(&sensor_data.timestamp);
    lpms_be1.get_acc(sensor_data.acc);
    lpms_be1.get_gyro(sensor_data.gyro);
    lpms_be1.get_quat(sensor_data.quat);
    lpms_be1.get_euler(sensor_data.euler);
}
void be1_print_data(void)
{
    float ts = (float)sensor_data.timestamp * 0.002;
    Serial.print("timestamp: ");
    Serial.println(ts, 3);

    Serial.print("acc: ");
    Serial.print(sensor_data.acc[0], 3);
    Serial.print(", ");
    Serial.print(sensor_data.acc[1], 3);
    Serial.print(", ");
    Serial.print(sensor_data.acc[2], 3);
    Serial.println();

    Serial.print("gyro: ");
    Serial.print(sensor_data.gyro[0], 3);
    Serial.print(", ");
    Serial.print(sensor_data.gyro[1], 3);
    Serial.print(", ");
    Serial.print(sensor_data.gyro[2], 3);
    Serial.println();

    Serial.print("quat: ");
    Serial.print(sensor_data.quat[0], 3);
    Serial.print(", ");
    Serial.print(sensor_data.quat[1], 3);
    Serial.print(", ");
    Serial.print(sensor_data.quat[2], 3);
    Serial.print(", ");
    Serial.print(sensor_data.quat[3], 3);
    Serial.println();

    Serial.print("euler: ");
    Serial.print(sensor_data.euler[0], 3);
    Serial.print(", ");
    Serial.print(sensor_data.euler[1], 3);
    Serial.print(", ");
    Serial.print(sensor_data.euler[2], 3);
    Serial.println();

    Serial.println();
}
